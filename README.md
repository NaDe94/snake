# README #

Thanks for checking out my Snake game!
The code is very simple and self explanatory(every method is commented using XML comments) and the gameplay itself should be well known :)

If you want to quickly test the game using cheats, create a file called "cheat.txt" is your asset (or game data) folder and put in the amount of snake tails you want to start out with.

Have fun, do whatever you like with the code. :)

----

Demo video: [https://www.youtube.com/watch?v=_u1RzpGnwck](https://www.youtube.com/watch?v=_u1RzpGnwck)
Blog post: [https://7bitstudios.wordpress.com/2016/01/09/snake-sharpfs/](https://7bitstudios.wordpress.com/2016/01/09/snake-sharpfs/)